#include <iostream>
#include <vector>
#include <unistd.h>
#include <chrono>
#include <sys/stat.h>
#include <sys/wait.h>
#include <map>
#include <semaphore.h>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <cstdlib>
#include <ctime>

using namespace std;

string trim(string& str)
{
    size_t first = str.find_first_not_of(' ');
    if (string::npos == first)
    {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

unsigned long getTime()
{
	unsigned long epochMilliseconds = chrono::duration_cast<chrono::milliseconds>
	(chrono::system_clock::now().time_since_epoch()).count();
	return epochMilliseconds;
}

typedef struct Road
{
	string src;
	string dst;
	int difficulty;
} Road;

typedef struct Path
{
	string route;
	int numOfCars;
} Path;

typedef struct CarInfo
{
	char entranceNode, exitNode;
	string entranceTime, exitTime;
	int localEmission, totalEmission;
} CarInfo;

vector <Road*> roads;
vector <Path*> paths;

class Monitor
{
private:
	vector <sem_t*> roadSemaphores;
	sem_t totalEmissionSemaphore;
	int totalEmission;

public:
	Monitor()
	{
		totalEmission = 0;
		sem_init(&totalEmissionSemaphore, 0, 1);
		for (int i = 0; i < roads.size(); i++)
		{
			sem_t* newSemaphore = new sem_t();
			sem_init(newSemaphore, 0, 1);
			roadSemaphores.push_back(newSemaphore);
		}
	}
	~Monitor()
	{
		for (int i = roadSemaphores.size() - 1; i >= 0; i--)
			sem_destroy(roadSemaphores[i]);
		sem_destroy(&totalEmissionSemaphore);
	}
	CarInfo* run(int index, int p)
	{
		CarInfo* newCarInfo = new CarInfo;
		int localEmission = 0;
		sem_wait(roadSemaphores[index]);
		newCarInfo -> entranceTime = to_string(getTime());
		for (int k = 0; k <= 10000000; k++)
		{
			localEmission += (int)(((double)k) / (1000000 * p * roads[index] -> difficulty));
		}
		sem_wait(&totalEmissionSemaphore);
		totalEmission += localEmission;
		newCarInfo -> totalEmission = totalEmission;
		sem_post(&totalEmissionSemaphore);
		newCarInfo -> entranceNode = roads[index] -> src[0];
		newCarInfo -> exitNode = roads[index] -> dst[0];
		newCarInfo -> localEmission = localEmission;
		newCarInfo -> exitTime = to_string(getTime());
		sem_post(roadSemaphores[index]);
		return newCarInfo;
	}
};

Monitor* monitor;

void readInput(string filename)
{
	string line, cell;
	bool filePart = false;
	ifstream inFile;
	inFile.open(filename);
	while(getline(inFile, line))
	{
		if(line[0] == '#')
		{
			filePart = true;
			continue;
		}
		stringstream lineStream(line);
		if(!filePart)
		{
			Road* R = new Road;

			getline(lineStream, cell, '-');
			R -> src = trim(cell);
			getline(lineStream, cell, '-');
			R -> dst = trim(cell);
			getline(lineStream, cell);
			R -> difficulty = stoi(trim(cell));
			roads.push_back(R);
		}
		else
		{
			Path* P = new Path;
			P -> route = "";

			while(getline(lineStream, cell, '-'))
				P -> route += trim(cell);
			
			getline(inFile, line);
			stringstream lineStream(line);
			
			getline(lineStream, cell);
			if(cell.size() == 0)
				break;
			P -> numOfCars = stoi(trim(cell));
			paths.push_back(P);
		}
		
	}
	inFile.close();
}

void threadRun(int pathIndex, int carNum)
{
	int p = rand() % 10 + 1;
	int localEmission;
	vector <CarInfo*> carInfos;
	char currentNode = paths[pathIndex]->route[0];
	for(int i = 1; i < paths[pathIndex]->route.size(); i++)
	{
		CarInfo* carInfo;
		for(int j = 0; j < roads.size(); j++)
		{
			if(roads[j]->src[0] == currentNode && roads[j]->dst[0] == paths[pathIndex]->route[i])
			{
				carInfo = monitor -> run(j, p);
				carInfos.push_back(carInfo);
				currentNode = paths[pathIndex]->route[i];
				break;
			}
		}
	}
	ofstream myFile;
	myFile.open(to_string(pathIndex) + "-" + to_string(carNum), ios::out);
	for (int i = 0; i < carInfos.size(); i++)
		myFile << carInfos[i]->entranceNode << "," << carInfos[i]->entranceTime << "," <<
		carInfos[i]->exitNode << "," << carInfos[i]->exitTime << "," <<
		carInfos[i]->localEmission << "," << carInfos[i]->totalEmission << endl;
	myFile.close();
}

int main(int argc, char *argv[])
{
	srand(time(0));
	readInput(argv[1]);
	vector <thread> threads;
	monitor = new Monitor();

	// for(int i = 0; i < roads.size(); i++)
	// 	cout << roads[i] -> src << " " << roads[i] -> dst << " " << roads[i] -> difficulty << endl;
	
	// for(int i = 0; i < paths.size(); i++)
	// 	cout << paths[i] -> route << " " << paths[i] -> numOfCars << endl;

	for(int i = 0; i < paths.size(); i++)
		for(int j = 0; j < paths[i]->numOfCars; j++)
			threads.push_back(thread(threadRun, i, j));
	
	for(int i = 0; i < threads.size(); i++)
		threads[i].join();

	return 0;
}
